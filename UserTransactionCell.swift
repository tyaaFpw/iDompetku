//
//  UserTransactionCell.swift
//  isat_idev
//
//  Created by Triyakom PT on 11/15/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import UIKit

class UserTransactionCell: UITableViewCell {
    
    @IBOutlet var transaction_img: UIImageView!
    @IBOutlet var txt_target: UILabel!
    @IBOutlet var txt_target_date: UILabel!
    @IBOutlet var txt_tr_value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
