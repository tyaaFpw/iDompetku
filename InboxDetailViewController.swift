//
//  InboxDetailViewController.swift
//  isat_idev
//
//  Created by Albert Januar on 12/7/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import SwiftyJSON
import UIKit

class InboxDetailViewController: UIViewController {
    
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    var content: JSON!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inbox"
        
        self.subjectLabel.text = self.content["trxType"].stringValue
        self.senderLabel.text = "Dari : Indosat Ooredo"// + self.content["userId"].stringValue
        self.timestampLabel.text = self.content["trxDateTime"].stringValue
        self.bodyLabel.text = self.content["trxLongMessage"].stringValue
    }
    
    @IBAction func detailButtonDidTapped(sender: AnyObject) {
    }
    
}
