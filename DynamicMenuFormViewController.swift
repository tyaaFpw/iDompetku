//
//  DynamicMenuFormViewController.swift
//  isat_idev
//
//  Created by Albert Januar on 12/7/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import ActionSheetPicker_3_0
import Alamofire
import SwiftyJSON
import UIKit

class DynamicMenuFormViewController: UIViewController {
    
    var type: DynamicMenuType = .DynamicMenuTypeBuy
    var formJSON: [JSON]?
    var forms: [UIView]!
    var msisdn: String?
    var pin: String?
    
    var ID: NSInteger = -1 {
        didSet {
            let parameters = ["id": String(format: "%ld", self.ID)]
            
            Alamofire.request(Method.POST, "http://103.200.4.20:8083/field/getfieldsfromID", parameters: parameters, encoding: ParameterEncoding.URL, headers: ["Content-Type": "application/x-www-form-urlencoded; charset=utf-8"]).response { (request, response, data, error) in
                if error == nil {
                    let json = JSON(data: data!)
                    self.formJSON = json.arrayValue.sort({ (jsonA, jsonB) -> Bool in
                        let posisiA = jsonA["posisi"].intValue
                        let posisiB = jsonB["posisi"].intValue
                        
                        return posisiA < posisiB
                    })
                    self.constructForm(self.formJSON!)
                }
            }
        }
    }
    
    func constructForm(formComponents: [JSON]) {
        var forms = [UIView]()
        for (index, element) in formComponents.enumerate() {
            var components = [UIView]()
            let view = UIView()
            components.append(self.createTitleLabel(element["nama"].stringValue))
            
            let type = element["type"].intValue
            if (type == 0) {
                let textField = self.createTextfield(element["hint"].stringValue, isSecure: false)
                textField.tag = index
                components.append(textField)
            }
            else if (type == 1) {
                let textField = self.createTextfield(element["hint"].stringValue, isSecure: false)
                textField.keyboardType = UIKeyboardType.NumberPad
                textField.tag = index
                components.append(textField)
            }
            else if (type == 2) {
                let textField = self.createTextfield(element["hint"].stringValue, isSecure: true)
                textField.tag = index
                components.append(textField)
            }
            else if (type == 3) {
                let prevalue = element["prevalue"].stringValue
                let textField = self.createTextFieldSelection(prevalue.componentsSeparatedByString(";").first!)
                textField.tag = index
                components.append(textField)
            }
            components.append(self.createErrorLabel())
            
            view.stackSubviewsVertically(components, insets: UIEdgeInsetsZero, spacing: 0, matchContent: true)
            forms.append(view)
        }
        self.view.stackSubviewsVertically(forms, insets: UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0), spacing: 10, matchContent: false)
        
        let submitButton = self.createSubmitButton()
        self.view.addSubview(submitButton)
        submitButton.snp_makeConstraints { (make) in
            make.top.equalTo(forms.last!.snp_bottom).inset(-10.0)
            make.centerX.equalTo(self.view)
        }
        
        self.forms = forms
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
    }

}

// UIKit creation
extension DynamicMenuFormViewController {
    
    private func createTextField() -> UITextField {
        let textField = UITextField()
        textField.font = UIFont.systemFontOfSize(14.0)
        textField.delegate = self
        return textField
    }
    
    func createTitleLabel(title: String) -> UILabel {
        let titleLabel = UILabel()
        titleLabel.font = UIFont.systemFontOfSize(14.0)
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.text = title
        return titleLabel
    }
    
    func createErrorLabel() -> UILabel {
        let errorLabel = UILabel()
        errorLabel.font = UIFont.systemFontOfSize(14.0)
        errorLabel.textColor = UIColor(red: 237.0/255.0, green: 28.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        return errorLabel
    }
    
    func createTextfield(placeholder: String, isSecure: Bool) -> UITextField {
        let textField = self.createTextField()
        textField.borderStyle = UITextBorderStyle.RoundedRect
        textField.secureTextEntry = isSecure
        return textField
    }
    
    func createTextFieldSelection(defaultValue: String) -> UITextField {
        let textField = self.createTextField()
        textField.text = defaultValue
        textField.borderStyle = UITextBorderStyle.Bezel
        return textField;
    }
    
    func createSubmitButton() -> UIButton {
        let button = UIButton()
        button.contentEdgeInsets = UIEdgeInsetsMake(8.0, 8.0, 8.0, 8.0)
        button.layer.cornerRadius = 5.0
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        button.titleLabel?.font = UIFont.boldSystemFontOfSize(14.0)
        button.backgroundColor = UIColor(red: 237.0/255.0, green: 28.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        button.addTarget(self, action: #selector(DynamicMenuFormViewController.submitButtonDidTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        if (self.type == .DynamicMenuTypeBuy) {
            button.setTitle("BELI", forState: UIControlState.Normal)
        }
        else if (self.type == .DynamicMenuTypePay) {
            button.setTitle("BAYAR", forState: UIControlState.Normal)
        }
        else {
            button.setTitle("TRANSFER", forState: UIControlState.Normal)
        }
        
        return button
    }
}

// Custom Method
extension DynamicMenuFormViewController {
    func submitButtonDidTapped(button: UIButton) {
        var validationPassed = true
        var parameters: [String: AnyObject] = [String: AnyObject]()
        
        for i in 0 ..< self.forms.count {
            let view = self.forms[i]
            let subviews = view.subviews
            let textField = subviews[1] as! UITextField
            let json = self.formJSON![textField.tag]
            
            if textField.text?.characters.count == 0 {
                validationPassed = false
                
                let errorLabel = subviews.last! as! UILabel
                errorLabel.text = json["errorText"].stringValue
            }
            else {
                let key = json["parameter"].stringValue
                if (key == "signature") {
                    parameters["PIN"] = textField.text!
                } else if (key == "denom") {
                    parameters[key] = textField.text!
                    parameters["amount"] = textField.text!
                } else {
                    parameters[key] = textField.text!
                }
            }
        }
        
        if (validationPassed) {
            let param = ["id": String(format: "%ld", self.ID)]
            
            Alamofire.request(Method.POST, "http://103.200.4.20:8083/api/getapifromIDmenu", parameters: param, encoding: ParameterEncoding.URL, headers: ["Content-Type": "application/x-www-form-urlencoded; charset=utf-8"]).response { (request, response, data, error) in
                if error == nil {
                    let json = JSON(data: data!)
                    var isNeedToRequestETC = false
                    
                    for i in 0..<json.arrayValue.count {
                        let newJSON = json.arrayValue[i]
                        if (newJSON["isDefaultSignature"].boolValue) {
                            isNeedToRequestETC = true
                        }
                    }
                    
                    if (isNeedToRequestETC) {
                        Alamofire.request(Method.POST, "http://103.200.4.20:8083/etc/viewetc", parameters: ["id": "1"], encoding: ParameterEncoding.URL, headers: ["Content-Type": "application/x-www-form-urlencoded; charset=utf-8"]).response { (request, response, data, error) in
                            if error == nil {
                                let etcJSON = JSON(data: data!)
                                self.msisdn = etcJSON["msisdn"].stringValue
                                self.pin = etcJSON["pin"].stringValue
                                self.formSubmission(0, formSubmission: json.arrayValue, parameters: parameters)
                            }
                        }
                    } else {
                        self.formSubmission(0, formSubmission: json.arrayValue, parameters: parameters)
                    }
                }
            }
        }
    }
    
    func formSubmission(index: NSInteger, formSubmission: [JSON], var parameters: [String: AnyObject]) {
        
        if (index < formSubmission.count) {
            let requiredParameter = formSubmission[index]["parameter"].stringValue.componentsSeparatedByString(";")
            let isDefaultSignature = formSubmission[index]["isDefaultSignature"].boolValue
            
            for i in 0..<requiredParameter.count {
                let keyString = requiredParameter[i]
                if parameters[keyString] == nil {
                    if (keyString == "amount") {
                        if (parameters["price"] != nil) {
                            parameters["amount"] = parameters["price"]
                        }
                        else {
                            parameters["amount"] = parameters["denom"]
                        }
                    }
                    else if (keyString == "transid") {
                        if (parameters["transid"] != nil) {
                            parameters["transid"] = parameters["transid"]
                        }
                        else {
                            parameters["transid"] = parameters["trxid"]
                        }
                    }
                    else if (keyString == "destname") {
                        parameters["destname"] = parameters["destAccName"]
                    }
                    else if (keyString == "bankName") {
                        parameters["bankName"] = parameters["destBankName"]
                    }
                    else if (keyString == "extref") {
                        parameters["extref"] = NSNumber(longLong: Int64(NSDate().timeIntervalSince1970))
                    }
                    else if (keyString == "msisdn") {
                        parameters["msisdn"] = Config.msisdn
                    }
                    else if (keyString == "signature") {
                        if (isDefaultSignature) {
                            parameters["signature"] = Util.createSignature(self.msisdn!, PIN: self.pin!)
                        } else {
                            parameters["signature"] = Util.createSignature(Config.msisdn, PIN: parameters["PIN"] as! String)
                        }
                    }
                }
            }

        }
        
        Alamofire.request(Method.POST, formSubmission[index]["host"].stringValue, parameters: parameters, encoding: ParameterEncoding.URL, headers: ["Content-Type": "application/x-www-form-urlencoded; charset=utf-8"]).response { (request, response, data, error) in
            
            if error == nil {
                let json = JSON(data: data!)
                let status = json["status"].intValue
                let message = json["msg"].stringValue
                
                print("json : %@", json)
                
                if status == 0 {
                    if (index + 1 < formSubmission.count) {
                        if (json["price"] != nil) {
                            parameters["price"] = json["price"].stringValue
                        }
                        if (json["trxid"] != nil) {
                            parameters["trxid"] = json["trxid"].stringValue
                        }
                        if (json["transid"] != nil) {
                            parameters["transid"] = json["transid"].stringValue
                        }
                        if (json["destAccName"] != nil) {
                            parameters["destAccName"] = json["destAccName"].stringValue
                        }
                        if (json["destBankName"] != nil) {
                            parameters["destBankName"] = json["destBankName"].stringValue
                        }
                        
                        self.formSubmission(index + 1, formSubmission: formSubmission, parameters: parameters)
                    }
                    else {
                        self.showErrorMessage(message, action: { (action) in
                            self.navigationController?.popToRootViewControllerAnimated(true)
                        })
                    }
                }
                else {
                    self.showErrorMessage(message, action: nil)
                }
            }
        }
    }
    
    func showErrorMessage(message: String, action: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: action))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}

extension DynamicMenuFormViewController: UITextFieldDelegate {
    
    // Calculating num of characters
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let json = self.formJSON![textField.tag]
        let numOfChars = json["jumlahKarakter"].intValue
        
        if (numOfChars == 0) {
            return true;
        }
        else {
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= numOfChars
        }
    }
    
    // Handle textField
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        let json = self.formJSON![textField.tag]
        let type = json["type"].intValue
        if (type == 3) {
            let prevalue = json["prevalue"].stringValue
            ActionSheetStringPicker.showPickerWithTitle(json["jumlah"].stringValue, rows: prevalue.componentsSeparatedByString(";"), initialSelection: 0, doneBlock: { (picer, index, value) in
                textField.text = value as? String
            }, cancelBlock: nil, origin: textField)
            return false
        }
        
        return true
    }
}