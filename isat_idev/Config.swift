//
//  Config.swift
//  isat_idev
//
//  Created by Albert Januar on 12/8/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import Foundation

struct Config {
    static var msisdn: String {
        get {
            let value = NSUserDefaults.standardUserDefaults().stringForKey("msisdn")
            return (value == nil) ? "" : value!
        }
        set {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "msisdn")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
}