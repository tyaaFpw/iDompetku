//
//  DIMOPayiOS.h
//  DIMOPayiOS
//
//

#import <UIKit/UIKit.h>

//! Project version number for DIMOPayiOS.
FOUNDATION_EXPORT double DIMOPayiOSVersionNumber;

//! Project version string for DIMOPayiOS.
FOUNDATION_EXPORT const unsigned char DIMOPayiOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DIMOPayiOS/PublicHeader.h>
#import <DIMOPayiOS/DIMOPay.h>
