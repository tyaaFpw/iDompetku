//
//  ShowBannerController.swift
//  isat_idev
//
//  Created by Awal on 12/13/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import UIKit
import Alamofire

class ShowBannerController: UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    var bannerID: String!
    
    
    func setupData(bannerID: String) {
        self.bannerID = bannerID
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Detail Promo"
        // Do any additional setup after loading the view.
        
        Alamofire.request(.GET, "https://mapi.dompetku.com:8082/promo/get_by_id/\(self.bannerID)").responseJSON { response in
            if let JSON = response.result.value {
                let items = JSON as! [NSDictionary]
                if items.count > 0 {
                    let item = items.first
                    
                    self.imgView.af_setImageWithURL(NSURL(string: "https://mapi.dompetku.com:8082\(item!["url_img"] as! String)")!)
                    self.lblTitle.text = item!["judul_promo"] as? String
                    self.txtDesc.text = item!["long_desc"] as! String
                }
                
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
