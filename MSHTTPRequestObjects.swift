 //
//  File.swift
//  MatchScores
//
//  Created by Triyakom PT on 4/26/16.
//  Copyright © 2016 Triyakom PT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

public typealias JsonResponse = (JSON?, String?) -> Void
public typealias ResponseAttr = (String?, String?) -> Void
public typealias ReverseString = (String?) -> Void
public typealias EncryptResult = (String?) -> Void
public typealias EncodeResult = (String?) -> Void
public typealias EncryptString = (String?, String?) -> Void

public class JSONObjects {
    
    var sessionID: String!
    public static let authurl = "https://mapi.dompetku.com/webapi"
    public static let balanceurl = "https://mapi.dompetku.com/webapiinternal"
    public static let with_port_url = "https://mapi.dompetku.com:8082/"
    public var curr_time = String()
    public var str_reverse = [String]()
    
    public var nextValues = Int()
    public var preValues = Int()
    public var just = String()
    public var select_id = String()
    public var testCookies2 = String()
    public var cookiesss = NSHTTPCookie()
    public let pref = NSUserDefaults.standardUserDefaults()
    public let m_type = NSUserDefaults.standardUserDefaults()
    public let pref2 = NSUserDefaults.standardUserDefaults()
    public let m_type2 = NSUserDefaults.standardUserDefaults()
    public let uidDefault = 0
    public var lastMatchID = String()
    public var mgr1: Alamofire.Manager!
    public let cookies1 = NSHTTPCookieStorage.sharedHTTPCookieStorage()
    
    public func shouldDoLogin(appendingURL: String, params:[String:AnyObject]?, onComplete: JsonResponse) {
            let url = "\(JSONObjects.balanceurl)/\(appendingURL)" //apending=balance_check
            Alamofire.request(.POST, url, parameters:params).response { request, response, data, error in
                if(data != nil) {
                    let json = JSON(data: data!)
                    print("\(url)&\(params)")
                    print("DATA: \(data)")
                    onComplete(json, nil)
                }
            }
    }
    
    public func doLogin(appendingURL: String, params:[String:AnyObject]?, onComplete: JsonResponse) {
        let url = "\(JSONObjects.authurl)/\(appendingURL)" //apending=balance_check
        Alamofire.request(.POST, url, parameters:params).response { request, response, data, error in
            if(data != nil) {
                let json = JSON(data: data!)
                print("\(url)&\(params)")
                print("DATA: \(data)")
                onComplete(json, nil)
            }
        }
    }
    
    public func urlWithPort(appendingURL: String, params: [String:AnyObject]?, onComplete: JsonResponse) {
        let url = "\(JSONObjects.with_port_url)/\(appendingURL)"
        Alamofire.request(.POST, url, parameters: params).response { (request, response, data, error) in
            if (data != nil) {
                let json = JSON(data: data!)
                print("\(url)&\(params)")
                print("DATA: \(data)")
                onComplete(json, nil)
            }
        }
    }
    
    public func getAllLoginAttributes(onComplete: ResponseAttr) {
//        let aStr = String(format: "%@%x", "timeNow in hex: ", timeNow)
        self.curr_time = String(format: "%.2f", NSDate().timeIntervalSince1970)
        
//        [NSString stringWithFormat:@"%0.0f",[[NSDate date] timeIntervalSince1970]]
//        self.curr_time = time_intv[time_intv.startIndex.advancedBy(0)...time_intv.startIndex.advancedBy(14)]
        
//        reversingString { (reverse: String?) in
//            onComplete(nil, reverse)
//        }
        onComplete(self.curr_time, nil)
    }
    
    public func reversingString(onComplete: ReverseString) {
        let pin = ["123456"]
        
//        let array = ["lizard", "Rhino", "Monkey"]
        self.str_reverse = pin.map() { String($0.characters.reverse()) }
//        print(reversed)
        onComplete(self.str_reverse.description)
    }
    
    public func encryptData(onComplete: EncryptResult) {
        let sign_A = "\(curr_time)\(123456)" //123456=dummy pin
        let sign_B = "654321|082112692727" //0815.. = dummy msisdn
        let sign_C = "\(sign_A)|\(sign_B)"
        
        self.tripleDesEncrypt(sign_C){ (reverse: String?, error: String?) in
            print("REVERSE: \(reverse)")
            onComplete(reverse)
        }
    }
    
    public func tripleDesEncrypt(pass: String, onComplete: EncryptString) -> String {
            //help from this thread
            //http://stackoverflow.com/questions/25754147/issue-using-cccrypt-commoncrypt-in-swift
        
        let keyString        = "EL_@51t4s_D0mp3Tku__REqV"
        let keyData: NSData! = (keyString as NSString).dataUsingEncoding(NSUTF8StringEncoding) as NSData!
        print("keyLength   = \(keyData.length), keyData   = \(keyData)")
        
//        let sign_A = "\(curr_time)\(123456)" //123456=dummy pin
//        let sign_B = "\(self.str_reverse[0])|\(081574448770)" //0815.. = dummy msisdn
//        let sign_C = "\(sign_A)|\(sign_B)"
        
        let message       = pass
        let data: NSData! = (message as NSString).dataUsingEncoding(NSUTF8StringEncoding) as NSData!
        print("data length = \(data.length), data      = \(data)")
        
        let cryptData    = NSMutableData(length: Int(data.length) + kCCBlockSizeAES128)!
        
        let keyLength              = size_t(kCCKeySizeAES256)
        let operation: CCOperation = UInt32(kCCEncrypt)
        let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES128)
        let options:   CCOptions   = UInt32(kCCOptionPKCS7Padding + kCCOptionECBMode)
        
        var numBytesEncrypted :size_t = 0
        
        let cryptStatus = CCCrypt(operation,
                                  algoritm,
                                  options,
                                  keyData.bytes, keyLength,
                                  nil,
                                  data.bytes, data.length,
                                  cryptData.mutableBytes, cryptData.length,
                                  &numBytesEncrypted)
        
        if UInt32(cryptStatus) == UInt32(kCCSuccess) {
            cryptData.length = Int(numBytesEncrypted)
            print("cryptLength = \(numBytesEncrypted), cryptData = \(cryptData)")
            
            // Not all data is a UTF-8 string so Base64 is used
            let base64cryptString = cryptData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            print("base64cryptString = \(base64cryptString)")
            self.encodeString(base64cryptString, onComplete: { (str_encode: String?) in
                onComplete(str_encode, nil)
            })
//            onComplete(base64cryptString, nil)
            
        } else {
            print("Error: \(cryptStatus)")
            onComplete(nil, String(cryptStatus))
        }
            return ""
    }
    
    func encodeString(str: String, onComplete: EncodeResult) -> String{
        let customAllowedSet =  NSCharacterSet(charactersInString:"==\n").invertedSet
        let escapedString = str.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
        print("escapedString: \(escapedString)")
        
        onComplete(escapedString)
        return escapedString!
    }
    
//    public func testLoginUsingBridgingMethod(username: String, password: String) {
//        var instanceLogin: NetraUSerProfile = NetraUserProfile()
//        var instanceOfCustomObject: CustomObject = CustomObject()
//        instanceOfCustomObject.someProperty = "Hello World"
//        println(instanceOfCustomObject.someProperty)
//        instanceOfCustomObject.someMethod()
//    }
    
//    func uploadingImage(image images: NSData) {
//        let parameters = [
//            "par1": "value",
//            "par2": "value2"]
//        
//        Alamofire.upload(.POST, JSONObjects.uploadURL, multipartFormData: {
//            multipartFormData in
//                    multipartFormData.appendBodyPart(data: images, name: "images")
//            
//            for (key, value) in parameters {
//                multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//            }
//            
//            }, encodingCompletion: {
//                encodingResult in
//                
//                switch encodingResult {
//                    
//                case .Success(let upload, _, _):
//                    upload.responseJSON { response in
//                            debugPrint(response)
//                    }
//                case .Failure(let encodingError):
//                    print(encodingError)
//                }
//        })
//    }
    
//    public func testCookie(uname uname: String, pwd: String, appleType: String, iosVer: String, mcc: String, carrier: String, phoneType: String, appVer: String) {
//        
//        let url = NSURL(string: "http://a.buzzmechat.com:5262/srv/mscore/auth?str=\(uname)|\(pwd)&str_dev=\(appleType)|\(phoneType))|\(iosVer)|\(mcc)|\(carrier)|\(appVer)")
//        let mutableUrlRequest = NSURLRequest(URL: url!)
//        
//        let prefs = NSUserDefaults.standardUserDefaults()
//        let cookie = prefs.valueForKey("COOKIE") as! String
//        print(cookie)
//        
//        Alamofire.request(mutableUrlRequest).responseJSON { (data: Response?)  in
//            print("DATA: \(data)")
//            
//        }
//    }
    
//    public func test (onComplete onComplete: JsonResponse) {
//        let params = ["str_dev":"testing|testing|testing|testing|testing|testing|testing"]
//        self.authenticateTest(params, onComplete: onComplete)
//    }
//    
//    public func authenticateTest (params:[String:AnyObject]?, onComplete: JsonResponse) {
//        let url = "http://a.buzzmechat.com:5262/srv/mscore/auth?str=yuna|yuna"
//        
//        Alamofire.request(.GET, url, parameters:params).responseJSON { response in
//            self.testcookie(response)
//            
//        }
//    }
//    
//    func testcookie(response: Response<AnyObject, NSError>) {
//        let cookies = NSHTTPCookie.cookiesWithResponseHeaderFields(response.response?.allHeaderFields as! [String: String], forURL: (response.response?.URL!)!)
//        print("COOKIE: \(cookies.description)")
//        
//        //Save method
//        self.setCookie(cookies.first!)
//    }
//    
//    func setCookie (cookie:NSHTTPCookie)
//    {
//        NSUserDefaults.standardUserDefaults().setObject(cookie.properties, forKey: "kCookie")
//        NSUserDefaults.standardUserDefaults().synchronize()
//    }
//        
//        func updateCookies(response: Response<AnyObject, NSError>) {
//            if let
//                headerFields = response.response?.allHeaderFields as? [String: String],
//                URL = response.request?.URL {
//                let cookies = NSHTTPCookie.cookiesWithResponseHeaderFields(headerFields, forURL: URL)
//                //print(cookies)
//                // Set the cookies back in our shared instance. They'll be sent back with each subsequent request.
//                Alamofire.Manager.sharedInstance.session.configuration.HTTPCookieStorage?.setCookies(cookies, forURL: URL, mainDocumentURL: nil)
//            }
//        }
//    
//    func fetchTheCookies() {
//        let parameters: [String: AnyObject] = [:]
//        
//        Alamofire.request(.POST, "http://example.com/LoginLocalClient", parameters: parameters).responseJSON { response in
//            if let
//                headerFields = response.response?.allHeaderFields as? [String: String],
//                URL = response.request?.URL
//            {
//                let cookies = NSHTTPCookie.cookiesWithResponseHeaderFields(headerFields, forURL: URL)
//                print(cookies)
//            }
//        }
//    }
//    
//    public func reqFromAPI2 (appendingURL: String, par: String, value: String, params:[String:AnyObject]?, onComplete: JsonResponse) {
//        let url = "\(JSONObjects.sec_url)\(appendingURL)?\(par)=\(value)"
//        Alamofire.request(.GET, url, parameters:params).response { request, response, data, error in
//            if(data != nil) {
//                print("\(url)&\(params)")
//                print("DATA: \(data)")
//                
//                let json = JSON(data: data!)
//                let needLogin = json["error"]
//                
//                if (needLogin == "need auth"){
//                    self.defaultAuthentication(){ (resp : JSON?, error: String?) in
//                        if error != nil {//authentikasi error
//                            onComplete(nil, error)
//                            print("AUTH_ERROR")
//                        }else{
//                            //REQUEST AGAIN
//                            self.requestingData(appendingURL, params:params){
//                                (data: JSON?, error:String?) in
//                                onComplete(data, error)
//                                print(data)
//                            }
//                        }
//                    }
//                }else{
//                    //Passing data to caller
//                    onComplete(json, nil)
//                }
//            }
//        }
//    }
//    
//    public func usingNewLoginRequest (appendingURL: String, params:[String:AnyObject]?, onComplete: JsonResponse) {
//        let url = JSONObjects.sec_url + appendingURL
//        Alamofire.request(.GET, url, parameters:params).response { request, response, data, error in
//            if(data != nil) {
//                let json = JSON(data: data!)
//                let needLogin = json["error"]
//                if (needLogin == ""){
////                    print("Need Login First")
//                    self.test(onComplete: { (resp: JSON?, error: String?) in
//                        if error != nil {//authentikasi error
//                            onComplete(nil, error)
//                            print("AUTH_ERROR")
//                        }else{
//                            //REQUEST AGAIN
//                            self.usingNewLoginRequest(appendingURL, params: params, onComplete: { (data: JSON?, error: String?) in
//                                print(data)
//                            })
//                        }
//                    })
//                }else{
//                    //Passing data to caller
//                    onComplete(json, nil)
//                    print("JSON: \(json)")
//                }
//            }
//        }
//    }
//    
//    public func requestingData (appendingURL: String, params:[String:AnyObject]?, onComplete: JsonResponse) {
//        let url = JSONObjects.urlBasic + appendingURL
//        Alamofire.request(.GET, url, parameters:params).response { request, response, data, error in
//            if(data != nil) {
//                let json = JSON(data: data!)
//                let needLogin = json["error"]
//                
//                if (needLogin == "need auth"){
//                    self.defaultAuthentication(){ (resp : JSON?, error: String?) in
//                        if error != nil {//authentikasi error
//                            onComplete(nil, error)
//                            print("AUTH_ERROR")
//                        }else{
//                            //REQUEST AGAIN
//                            self.requestingData(appendingURL, params:params){
//                                (data: JSON?, error:String?) in
//                                onComplete(data, error)
//                                print(data)
//                            }
//                        }
//                    }
//                }else{
//                    //Passing data to caller
//                    onComplete(json, nil)
//                }
//            }
//        }
//    }
//    
//    public func requestingData2 (appendingURL: String, params:[String:AnyObject]?, param1: String , param2: String, onComplete: JsonResponse) {
//        let url = JSONObjects.sec_url + appendingURL //appendingURL = feedback?feed=(isi feedbackmssg)
//        NSHTTPCookieStorage.sharedHTTPCookieStorage().setCookie(getCookiess())
//        Alamofire.request(.GET, url, parameters:params).response { request, response, data, error in
//            if(data != nil) {
//                let json = JSON(data: data!)
//                let needLogin = json["err_msg"]
//                
//                if (needLogin == "need_auth"){
////                    let paramtr = ["str_dev":"testing|testing|testing|testing|testing|testing|testing"]
//                    
////                    self.newRequest(appendingURL, params: params, param1: param1, param2: param2, onComplete: { (data: JSON?, error: String?) in
////                        if error != nil {//authentikasi error
////                            onComplete(nil, error)
////                            print("AUTH_ERROR")
////                        }else{
////                            //REQUEST AGAIN
////                            print("Success")
//////                            self.requestingData2(appendingURL, params: params, param1: param1, param2: param2, onComplete: { (data: JSON?, error: String?) in
//////                                onComplete(data, error)
//////                                print("request2: \(data)")
//////                                if let
//////                                    headerFields = response?.allHeaderFields as? [String: String],
//////                                    URL = response?.URL
//////                                {
//////                                    let cookies = NSHTTPCookie.cookiesWithResponseHeaderFields(headerFields, forURL: URL)
//////                                    print("COOKIE2: \(cookies)")
//////                                }
//////                            })
////                        }
////                    })
//                }else{
//                    //Passing data to caller
////                    if let
////                        headerFields = response?.allHeaderFields as? [String: String],
////                        URL = response?.URL
////                    {
////                        let cookies = NSHTTPCookie.cookiesWithResponseHeaderFields(headerFields, forURL: URL)
////                        print("COOKIE2: \(cookies)")
////                        self.requestingData2(appendingURL, params: params, param1: param1, param2: param2, onComplete: { (data: JSON?, error: String?) in
////                            onComplete(data, error)
////                            print("request2: \(data)")
////                        })
////                    }
//                    onComplete(json, nil)
//                }
//            }
//        }
//    }
//    
//    public func newAuth(type: String, appendingURL: String, login_params:[String:AnyObject]?, param1: String, param2: String, onComplete: JsonResponse) {
////        let urlx = JSONObjects.newURL + "\(param1)" + "&str_dev=" + "\(param2)" //appendingURL = username|password
//        let urlx = JSONObjects.auth + appendingURL
//        Alamofire.request(.GET, urlx, parameters: login_params).response { request, response, data, error in
//            if(data != nil) {
//                let json = JSON(data: data!)
//                print("JSON: \(json)")
//                var user_sessid = json["user"]["session_id"].stringValue
//                user_sessid = "JSESSIONID=\(user_sessid)"
//                self.test(test: user_sessid, appendingURL: appendingURL, onComplete: { (data: JSON?, error: String?) in
//                    onComplete(data, nil)
//                })
//            } else {
//                print("NO DATA")
//                onComplete(nil, "Error login")
//            }
//        }
//    }
//    
////    public func reqAppTyk(type: String, appendingURL: String, params:[String:AnyObject]?, onComplete: JsonResponse) {
////        //http://app.triyakom.com/sport/feedback?feed=testing&cat=1
////        //ex of "type" is = feedback?feed=
////        //ex of "appendURL = testing"
////        let url2 = JSONObjects.sec_url + type + appendingURL
////        //        let url = JSONObjects.sec_url + "feedback?feed=testing&cat=1"
////        
////        let urls = NSURL(string: url2)
////        let mutableURLRequest = NSMutableURLRequest(URL: urls!)
////        mutableURLRequest.HTTPMethod = "GET"
////        mutableURLRequest.setValue(test, forHTTPHeaderField: "Cookie")
////        
////        let manager = Alamofire.Manager.sharedInstance // or create a new one
////        let request = manager.request(mutableURLRequest)
////        request.response { (request, response, data, error) in
////            if (data != nil) {
////                let json = JSON(data: data!)
////                print("DATA:: \(json)")
////            } else {
////            }
////        }
////        
////        Alamofire.request(.GET, url2, parameters:params).response { request, response, data, error in
////            
////        }
////    }
//    
//    //NSURL(string: "http://app.triyakom.com/sport/feedback?feed=testing&cat=1")
//    public func newRequest(appendingURL: String, params:[String:AnyObject]?, param1: String, param2: String, onComplete: JsonResponse) {
//        let urlx = JSONObjects.auth + "\(param1)" + "&str_dev=" + "\(param2)" //appendingURL = username|password
//        Alamofire.request(.GET, urlx).response { (request, response, data, error) in
//            if(data != nil) {
//                let json = JSON(data: data!)
//                print("JSON: \(json)")
//                var user_sessid = json["user"]["session_id"].stringValue
//                user_sessid = "JSESSIONID=\(user_sessid)"
//                self.test(test: user_sessid, appendingURL: appendingURL, onComplete: { (data: JSON?, error: String?) in
//                    onComplete(data, nil)
//                })
//                
////                onComplete(json, nil)
//            } else {
//                print("NO DATA")
//                onComplete(nil, "Error login")
//            }
//        }
//    }
//    
//    func test(test test: String, appendingURL: String, onComplete: JsonResponse) {
//        print(test)
//        if (test != "") {
//            let url = JSONObjects.sec_url + appendingURL //appendingURL = feedback?feed=(isi feedbackmssg)
//            let urls = NSURL(string: url)
//            let mutableURLRequest = NSMutableURLRequest(URL: urls!)
//            mutableURLRequest.HTTPMethod = "GET"
//            mutableURLRequest.setValue(test, forHTTPHeaderField: "Cookie")
//            
//            let manager = Alamofire.Manager.sharedInstance // or create a new one
//            let request = manager.request(mutableURLRequest)
//            request.response { (request, response, data, error) in
//                if (data != nil) {
//                    let json = JSON(data: data!)
//                    print("DATA:: \(json)")
//                    onComplete(json, nil)
//                } else {
//                }
//            }
//        } else {
//            print("No Cookie")
//        }
//    }
//    
////    func test1() {
////        let URL = NSURL(string: query)!
////        let mutableUrlRequest = NSMutableURLRequest(URL: URL)
////        mutableUrlRequest.HTTPMethod = "GET"
////        
////        let prefs = NSUserDefaults.standardUserDefaults()
////        let cookie = prefs.valueForKey("COOKIE") as String
////        
////        mutableUrlRequest.setValue(cookie, forHTTPHeaderField: "Cookie")
////        
////        Alamofire.request(mutableUrlRequest).responseJSON { (request, response, data, error) -> Void in
////            
////            //handling the response       
////        }
////    }
//    
//    func setCookiess (cookie:NSHTTPCookie)
//    {
//        let encodedData = NSKeyedArchiver.archivedDataWithRootObject(cookie.properties!)
//        self.pref2.setObject(encodedData, forKey: "Cookies")
//        self.pref2.synchronize()
//    }
//    
//    func getCookiess () -> NSHTTPCookie
//    {
//        self.cookiesss = NSHTTPCookie(properties: self.pref2.objectForKey("Cookies") as! [String : AnyObject])!
//        return self.cookiesss
//    }
//
//    //TODO prevent loop when server error, auth success but request after it is error need auth
//    public func defaultAuthentication(onComplete : JsonResponse){
//        //TODO set uid
//        if let highscore = self.pref.valueForKey("userid") {
//            // do something here when a highscore exists
//            print(highscore)
//            //            let uid = "0"
//            let authUrl = JSONObjects.urlBasic + "auth&uid=" + (highscore as! String) + "&appid=4"
//            Alamofire.request(.GET, authUrl).response{ (request, response, data, error) in
//                if(data != nil){
//                    let json = JSON(data: data!)
//                    self.sessionID = json["sessionid"].string
//                    onComplete(nil, nil)
//                    
//                }else{
//                    onComplete(nil, "Error login")
//                }
//            }
//        } else {
//            let uid = "0"
//            let authUrl = JSONObjects.urlBasic + "auth&uid=" + uid + "&appid=4"
//            Alamofire.request(.GET, authUrl).response{ (request, response, data, error) in
//                if(data != nil){
//                    let json = JSON(data: data!)
//                    self.sessionID = json["sessionid"].string
//                    onComplete(nil, nil)
//                    
//                }else{
//                    onComplete(nil, "Error login")
//                }
//            }
//        }
//    }
//    
////    public func userAuthentication(onComplete : JsonResponse){
////        //TODO set uid
////        if let highscore = self.pref.valueForKey("userid") {
////            // do something here when a highscore exists
////        }
////        let authUrl = JSONObjects.urlBasic + "auth&uid=" + uid + "&appid=4"
////        Alamofire.request(.GET, authUrl).response{ (request, response, data, error) in
////            if(data != nil){
////                let json = JSON(data: data!)
////                self.sessionID = json["sessionid"].string
////                onComplete(nil, nil)
////                
////            }else{
////                onComplete(nil, "Error login")
////            }
////        }
////    }
//    
//    public func testConnection(appendingURL appendingURL: String, params: [String:AnyObject]?, onComplete: JsonResponse) {
//        let url = JSONObjects.urlBasic + appendingURL
//        Alamofire.request(.GET, url, parameters: params).response { request, response, data, error in
//            if(data != nil) {
//                let json = JSON(data: data!)
//                let message = json["error"]
//                
//                if (message == "need auth"){
//                    self.defaultAuthentication(){ (resp: JSON?, error: String?) in
//                        if error != nil {
//                            onComplete(nil, error)
//                            print("AUTH_ERROR")
//                        } else {
//                            self.requestingData(appendingURL, params: params){ (data: JSON?, error: String?) in
//                                onComplete(data, error)
//                                print("NEW REQUEST")
//                                print(data)
//                                if let json = data {
//                                    self.nextValues = json["has_next"].intValue
//                                    self.preValues = json["has_prev"].intValue
//                                }
//                            }
//                        }
//                    }
//                } else {
//                    onComplete(json, nil)
//                }
//            }
//        }
//    }
//    
////    public func connectingF1GPNextPrev(appendingURL: String, params:[String:AnyObject]?, onComplete: JsonResponse) {
////        let url = JSONObjects.urlBasic + appendingURL
////        Alamofire.request(.GET, url, parameters:params).response { request, response, data, error in
////            if(data != nil) {
////                let json = JSON(data: data!)
////                print("json: \(json)")
////            } else {
////                print("NO DATA FROM SERVER")
////            }
////        }
////    }
//    
//    func requestLastMatch(onComplete : (LastMatchModel)->()){
//        requestingData("lmrandom", params:nil){
//            (data: JSON?, error:String?) in
//            var lm = LastMatchModel()
//            
//            if let json = data {
//                for item in json["list"].arrayValue {
//                    let homename = item["home_team"].stringValue
//                    let awayname = item["away_team"].stringValue
//                    let homescore = item["home_score"].stringValue
//                    let awayscore = item["away_score"].stringValue
//                    let homeimg = item["home_url"].stringValue
//                    let awayimg = item["away_url"].stringValue
//                    
//                    lm.lasthomename.append(homename)
//                    lm.lastawayname.append(awayname)
//                    lm.lasthomescore.append(homescore)
//                    lm.lastawayscore.append(awayscore)
//                    lm.lasthomeimg.append(homeimg)
//                    lm.lastawayimg.append(awayimg)
//                }
//                onComplete(lm)
//            }
//        }
//    }
//    
//    
//    
}

 struct Selected {
    var match_id = String()
 }
