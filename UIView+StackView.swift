//
//  UIView+StackView.swift
//  isat_idev
//
//  Created by Albert Januar on 12/12/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import SnapKit
import UIKit

extension UIView {
    func stackSubviewsHorizontally(subviews: [UIView], insets: UIEdgeInsets, spacing: CGFloat) {
        for (index, subview) in subviews.enumerate() {
            self.addSubview(subview)
            
            if (index == 0) {
                subview.snp_makeConstraints(closure: { (make) in
                    make.leading.equalTo(self).offset(insets.left)
                })
            }
            else {
                subview.snp_makeConstraints(closure: { (make) in
                    make.leading.equalTo(subviews[index - 1].snp_trailing).offset(spacing)
                })
            }
            
            subview.snp_makeConstraints(closure: { (make) in
                make.top.equalTo(self).offset(insets.top)
                make.bottom.equalTo(self).offset(-insets.bottom)
            })
            
            if (index == subviews.count - 1) {
                subview.snp_makeConstraints(closure: { (make) in
                    make.trailing.equalTo(self).inset(-insets.right)
                })
            }
        }
        self.layoutIfNeeded()
    }
    
    func stackSubviewsVertically(subviews: [UIView], insets: UIEdgeInsets, spacing: CGFloat, matchContent: Bool) {
        for (index, subview) in subviews.enumerate() {
            self.addSubview(subview)
            
            if (index == 0) {
                subview.snp_makeConstraints(closure: { (make) in
                    make.top.equalTo(self).offset(insets.top)
                })
            }
            else {
                subview.snp_makeConstraints(closure: { (make) in
                    make.top.equalTo(subviews[index - 1].snp_bottom).offset(spacing)
                })
            }
            
            subview.snp_makeConstraints(closure: { (make) in
                make.leading.equalTo(self).offset(insets.left)
                make.trailing.equalTo(self).offset(-insets.right)
            })
            
            if (matchContent && index == subviews.count - 1) {
                subview.snp_makeConstraints(closure: { (make) in
                    make.bottom.equalTo(self).inset(-insets.bottom)
                })
            }
        }
        self.layoutIfNeeded()
    }
}
