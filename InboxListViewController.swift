//
//  InboxListViewController.swift
//  isat_idev
//
//  Created by Albert Januar on 12/7/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

class InboxListViewController: UITableViewController {
    
    var inbox: [JSON] = [JSON]()
    var inbox2: [JSON] = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inbox"
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tableView.estimatedRowHeight = 80.0
        self.tableView.registerNib(UINib(nibName: "InboxListCell", bundle: nil), forCellReuseIdentifier: "InboxListCell")
        
        self.refreshData()
    }
    
    
    func refreshData() {
        let parameters = ["userId": "082112692727"]//Config.phoneNumber]
        
        Alamofire.request(Method.GET, "https://mapi.dompetku.com/viewtransaction/v1/getInboxList", parameters: parameters, encoding: ParameterEncoding.URL, headers: ["Content-Type": "application/json"]).response { (request, response, data, error) in
            if error == nil {
                let json = JSON(data: data!)
                self.inbox = json["data"].arrayValue
                
                if let myLoadedString = NSUserDefaults.standardUserDefaults().stringForKey("final_sign") {
                    let url = "http://103.200.4.20:8083/message/get_inbox"
                    let params = ["signature":"bjbzah+NWWHhuGHhgwdKHouhFNmK2Z7zc7Mv4xbOsJTxmw1c4D6r8Q=="]
                    Alamofire.request(.POST, url, parameters:params).response { request, response, data, error in
                        if(data != nil) {
                            let json = JSON(data: data!)
                            print("Inbox Kedua: \(json)")
                            var dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                            dataString = dataString!.stringByReplacingOccurrencesOfString("subject", withString: "trxType")
                            dataString = dataString!.stringByReplacingOccurrencesOfString("timestamp", withString: "trxDateTime")
                            dataString = dataString!.stringByReplacingOccurrencesOfString("body", withString: "trxLongMessage")
                            let data = dataString!.dataUsingEncoding(NSUTF8StringEncoding)
                            print("Inbox Kedua: \(JSON(data: data!))")
                            self.inbox2 = JSON(data: data!).arrayValue
                            self.inbox += self.inbox2
                            self.tableView.reloadData()
                        }
                    }
                }
                
                
            }
        }
        
    }
    
    
}

// MARK: - UITableViewDataSource
extension InboxListViewController {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inbox.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("InboxListCell") as? InboxListCell
        cell?.senderLabel.text = "Indosat Ooredo"//self.inbox[indexPath.row]["userId"].stringValue
        cell?.subjectLabel.text = self.inbox[indexPath.row]["trxType"].stringValue
        return cell!
    }
}

extension InboxListViewController {
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let inboxDetailViewController = InboxDetailViewController(nibName: "InboxDetailViewController", bundle: nil)
        inboxDetailViewController.content = self.inbox[indexPath.row]
        self.navigationController?.pushViewController(inboxDetailViewController, animated: true)
    }
}
