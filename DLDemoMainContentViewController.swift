//
//  DLDemoMainContentViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import AlamofireImages
import QuartzCore
//import KYCircularProgress

class DLDemoMainContentViewController: UIViewController, BLPageableScrollViewDataSource {
    
    @IBOutlet var banner_img: UIImageView!
    @IBOutlet var bannerView: UIView!
    @IBOutlet var banner_ctrl: UIPageControl!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var midbtnView: UIView!
    @IBOutlet var midbtn: UIButton!
    
    @IBOutlet var login_btn: UIButton!
    @IBOutlet var no_loginView: UIView!
    @IBOutlet var already_loginView: UIView!
    let objects = JSONObjects()
    var datas = LoginData()
    
//    let titles = ["Cari", "Agen Terdekat"]//, "Sharing"]
//    let title_desc = ["Cari promo berdasarkan nama", "Lihat dan cari agen Do terdekat dengan lokasi Anda"]
//    let images = ["search.png", "nearby_agent.png"]//, ["facebook.png", "twitter.png", "ok.png"]]
    let banner_array = ["banner_1.jpg", "banner_2.jpg", "banner_3.jpg"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        appearingMenuController()
        
        self.roundedUIView()
        self.loginAttribute()
        let logoImage = UIImageView(frame: CGRect(x:0, y:0, width: 200, height: 45))
        logoImage.image = UIImage(named: "profile_pict.png")
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController!.navigationItem.titleView = logoImage
        self.navigationController!.navigationBar.barTintColor = UIColor.redColor()
        
        NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(DLDemoMainContentViewController.testChangingBG), userInfo: nil, repeats: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @IBAction func goToSecond(sender: AnyObject) {
//        if tabBarController?.selectedIndex == 2 {
//            self.presentViewController(ResultsViewController(), animated: true, completion: nil)
//        } else if tabBarController?.selectedIndex == 1 {
//            //            self.presentViewController(DLDemoMainContentViewController(), animated: true, completion: nil)
//            print("Do you call me??")
//        }
//    }
    
    func loginAttribute() {
        self.login_btn.layer.cornerRadius = login_btn.frame.size.width/5
    }
    
    func roundedUIView() {
        self.midbtnView.layer.cornerRadius = midbtnView.frame.size.width/2
        self.midbtn.layer.cornerRadius = midbtn.frame.size.width/2
    }
    
    func configurePageControl(page page: Int) {
        self.banner_ctrl.numberOfPages = self.banner_array.count
        self.banner_ctrl.currentPage = page
        self.banner_ctrl.tintColor = UIColor.redColor()
        self.banner_ctrl.currentPageIndicatorTintColor = UIColor.redColor()
    }
    
    func testChangingBG() {
        let random = Int(arc4random_uniform(UInt32(self.banner_array.count)))
        if random == 0 {
            print("index 0")
            self.banner_ctrl.currentPage = 0
        } else if random == 1 {
            print("index 1")
            self.banner_ctrl.currentPage = 1
        } else if random == 2 {
            print("index 2")
            self.banner_ctrl.currentPage = 2
        } else { print("Out of index") }
        self.banner_img.image = UIImage(named: self.banner_array[random])
        print("Image_name:: \(random)")
    }
    
 
//    @IBAction func menuTouched(sender: AnyObject) {
//        self.findHamburguerViewController()?.showMenuViewController()
//    }
    
    @IBAction func showingLeftMenu(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
    }

    @IBAction func doLogin(sender: AnyObject) {
        let params = ["userid":"mobile_apps_elasitas", "to":"082112692727"]
        self.objects.shouldDoLogin("balance_check", params: params) { (data: JSON?, error: String?) in
            if let json = data {
                print("Login Result: \(json)")
                if (json["status"].stringValue == "0") {
                    Config.msisdn = params["to"]!
                    self.loginTest()
                } else if (json["status"].stringValue == "-11") {
                    print("This user is unregistered. you have register first..")
                } else { print("Invalid status.") }
            } else {
            print("Data")
            }
        }
    }
    
//    @IBAction func menuButtonTouched(sender: AnyObject) {
//        self.findHamburguerViewController()?.showMenuViewController()
//    }
    
//    func appearingMenuController() {
//        if revealViewController() != nil {
//            menuButton.target = revealViewController()
//            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
//            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        }
//    }
    
    func loginTest() {
        //        self.objects.getAllLoginAttributes(){(result: String?, error: String?) in
        ////            print("REVERSE: \(error)")
        //            print("RESULT: \(result)")
        //        }
        //        self.objects.reversingString { (reverse: String?) in
        //            print("REVERSE: \(reverse)")
        //        }
        //
        //        self.objects.encryptData{ (enc: String?) in
        //            print("ENCRYPTED: \(enc)")
        //            let params = ["signature": "\(enc)", "userid":"mobile_apps_elasitas"]
        //            self.objects.doLogin("login", params: params, onComplete: { (data: JSON?, error: String?) in
        //                if let json = data {
        //                    print("Login Result: \(json)")
        //                }
        //            })
        //        }
        
        NetraUserProfile.login()
        if let myLoadedString = NSUserDefaults.standardUserDefaults().stringForKey("final_sign") {
            print(myLoadedString) // "Hello World"
            let params = ["signature": "\(myLoadedString)", "userid":"mobile_apps_elasitas"]
            self.objects.doLogin("login", params: params, onComplete: { (data: JSON?, error: String?) in
                if let json = data {
                    print("Login Result: \(json)")
                    self.datas = LoginData(status: json["status"].stringValue, msg: json["msg"].stringValue)
                    
                    if (json["status"].stringValue == "0") {
                        self.no_loginView.hidden = true
                        self.already_loginView.hidden = false
                    }
                }
            })
        }
    }
    
    func pageableScrollView(pageableScrollView: BLPageableScrollView, didSelectAtIndex index: Int) {
        print(index)
    }
    
    func pageableScrollView(pageableScrollView: BLPageableScrollView, viewAtIndex index: Int) -> UIView {
        let view: UIView = pageableScrollView.dequeueReusableViewWithIdentifier("view")!
//         = self.banner_array[index]
        return view
    }
    
    func numberOfPagesInPageableScrollView(pageableScrollView: BLPageableScrollView) -> Int {
        return 3
    }
    
//    //table view
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return 1
//    }
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.titles.count
//    }
//    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        //print("I'm selecting the home Activity Menu..")
//        if (indexPath.row == 0) {
//            
//        }
//        else if (indexPath.row == 1) {
//            let nearAgentController = NearAgentController(nibName: "NearAgentController", bundle: nil)
//            self.navigationController?.pushViewController(nearAgentController, animated: true)
//        }
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("main_activity_menu_cell", forIndexPath: indexPath) as? MainActivityMenuCell
//        cell?.menu_title.text = self.titles[indexPath.row]
//        cell?.menu_title_desc.text = self.title_desc[indexPath.row]
//        cell?.menu_img.image = UIImage(named: self.images[indexPath.row])
//        return cell!
//    }
}

struct LoginData {
    var status = String()
    var msg = String()
}

extension DLDemoMainContentViewController {

    // MARK: - For Mail
    @IBAction func mailButtonDidTapped(sender: AnyObject) {
        let inboxListViewController = InboxListViewController()
        self.navigationController?.pushViewController(inboxListViewController, animated: true)
    }
    
    // MARK: - For Dynamic menu
    @IBAction func payButtonDidTapped(sender: AnyObject) {
        DynamicMenuViewController.showDynamicMenuViewController(self, ID: 51)
    }
    
    @IBAction func buyButtonDidTapped(sender: AnyObject) {
        DynamicMenuViewController.showDynamicMenuViewController(self, ID: 50)
    }
    
    @IBAction func transferButtonDidTapped(sender: AnyObject) {
        DynamicMenuViewController.showDynamicMenuViewController(self, ID: 52)
    }
    
}
