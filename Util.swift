//
//  Util.swift
//  isat_idev
//
//  Created by Albert Januar on 12/14/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import Foundation

class Util: NSObject {
    
    class func createSignature(msisdn: String, PIN: String) -> String {
        let key = "EL_@51t4s_D0mp3Tku__REqV"
        let timestamp = String(format: "%0.0f", NSDate().timeIntervalSince1970)
        let signA = timestamp + PIN
        let signB = String(format: "%@|%@", String(PIN.characters.reverse()), msisdn)
        let signC = String(format: "%@|%@", signA, signB)
        
        return NetraUserProfile.TripleDES(signC, encryptOrDecrypt: UInt32(kCCEncrypt), key: key)
    }
    
}