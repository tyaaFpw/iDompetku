//
//  DLDemoMenuViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit
import Social

class DLDemoMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // outlets
    @IBOutlet weak var tableView: UITableView!
    var selectedMenu: String = ""
    let segues = ["Cara Top-up", "Informasi", "Tentang Dompetku", "Hubungi Kami"]//, "Sharing"]
    let imageName = ["topup.png", "informasi.png", "about.png", "contact"]//, ["facebook.png", "twitter.png", "ok.png"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.backgroundColor = UIColor.clearColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: UITableViewDelegate&DataSource methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.segues.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath) as? SideMenuTableCell
        cell?.menu_title.text = self.segues[indexPath.row]
        cell?.menu_image.image = UIImage(named: imageName[indexPath.row])
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Side menu selected..")
//        let nvc = self.mainNavigationController()
//        let abc = self.fillSection[indexPath.section][indexPath.row]
//        self.objects.pref.setValue(self.fillSection[indexPath.section][indexPath.row], forKey: "tournament_name")
//        if let hamburguerViewController = self.findHamburguerViewController() {
//            hamburguerViewController.hideMenuViewControllerWithCompletion({ () -> Void in
//                if (abc == "Premiere League" || abc == "La Liga" || abc == "Champions" || abc == "European Football Championship" || abc == "Indonesia Super League" || abc == "World Cup" || abc == "Bundesliga" || abc == "Italia Serie A") {
//                    nvc.visibleViewController!.performSegueWithIdentifier("football", sender: nil)
//                } else {
//                    nvc.visibleViewController!.performSegueWithIdentifier(abc, sender: nil)
//                }
//                hamburguerViewController.contentViewController = nvc
//            })
//        }
    }
    
    //football
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
//        
//        if (segue.identifier == "football") {
//            // initialize new view controller and cast it as your view controller
//            let viewController = segue.destinationViewController as! FourthController
//            let viewController2 = segue.destinationViewController as! ResultsViewController
//            if let selectedIndexPath =  self.tableView.indexPathForSelectedRow{
//                viewController.match.match_id = String(selectedIndexPath.row)
//                viewController.match.match_type = self.fillSection[selectedIndexPath.section][selectedIndexPath.row]
//            }
//            
//            if let selectedIndexPath =  self.tableView.indexPathForSelectedRow{
////                viewController2.selected_match.match_type = String(selectedIndexPath.row)
//                viewController2.selected_match.match_type = self.fillSection[selectedIndexPath.section][selectedIndexPath.row]
//            }
//        }
//    }
    
    
    // MARK: - Navigation
    func mainNavigationController() -> DLHamburguerNavigationController {
        return self.storyboard?.instantiateViewControllerWithIdentifier("DLDemoNavigationViewController") as! DLHamburguerNavigationController
    }
    
}
