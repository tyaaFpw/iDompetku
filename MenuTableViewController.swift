//
//  MenuTableViewController.swift
//  isat_idev
//
//  Created by Albert Januar on 12/2/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import Alamofire
import AlamofireImage
import UIKit
import SwiftyJSON

class MenuTableViewController: UITableViewController {
    
    var type: DynamicMenuType = .DynamicMenuTypeBuy
    
    private var menus: [JSON] = [JSON]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    init(ID: NSInteger) {
        super.init(style: UITableViewStyle.Plain)
        self.refreshData(ID)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorInset = UIEdgeInsetsZero
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tableView.rowHeight = 48.0
        self.tableView.estimatedRowHeight = 48.0
        self.tableView.registerNib(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
    }
    
    func refreshData(ID: NSInteger) {
        let parameters = ["id": String(format: "%ld", ID)]
        
        Alamofire.request(Method.POST, "http://103.200.4.20:8083/menucms/getchildallbyid", parameters: parameters, encoding: ParameterEncoding.URL, headers: ["Content-Type": "application/x-www-form-urlencoded; charset=utf-8"]).response { (request, response, data, error) in
            if error == nil {
                let json = JSON(data: data!)
                self.menus = json.arrayValue
            }
        }
    }
}

// MARK: - UITableViewDatasource
extension MenuTableViewController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menus.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell") as? MenuCell
        cell?.leftImageView?.af_setImageWithURL(NSURL(string: self.menus[indexPath.row]["image_url"].stringValue)!)
        cell?.titleLabel?.text = self.menus[indexPath.row]["nama"].stringValue
        return cell!
    }
}

// MARK: - UITableViewDelegate
extension MenuTableViewController {
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let json = self.menus[indexPath.row];
        if (!json["isLeaf"].boolValue) {
            let tableViewController = MenuTableViewController(ID: json["id"].intValue)
            tableViewController.type = self.type
            self.navigationController?.pushViewController(tableViewController, animated: true)
        }
        else {
            let formViewController = DynamicMenuFormViewController()
            formViewController.ID = json["id"].intValue
            formViewController.type = self.type
            self.navigationController?.pushViewController(formViewController, animated: true)
        }
    }
}