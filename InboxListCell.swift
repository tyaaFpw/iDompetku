//
//  InboxListCell.swift
//  isat_idev
//
//  Created by Albert Januar on 12/7/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import UIKit

class InboxListCell: UITableViewCell {
    
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    
}
