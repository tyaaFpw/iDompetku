//
//  PromoTableViewController.swift
//  isat_idev
//
//  Created by Fitrah Ramadhan on 12/12/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

class PromoTableViewController: UITableViewController {
    
    var promo = []
    var isDetail: Bool = false
    var categoryID: String!
    
    func setupData(categoryID: String!, isDetail: Bool) {
        self.categoryID = categoryID
        self.isDetail = isDetail
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Promo"
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tableView.estimatedRowHeight = 150.0
        self.tableView.registerNib(UINib(nibName: "PromoListCell", bundle: nil), forCellReuseIdentifier: "PromoListCell")
        self.refreshData()
    }
    
    func refreshData() {
        
        if self.isDetail {
            print("https://mapi.dompetku.com:8082/promo/get_by_id_category/\(self.categoryID)")
            Alamofire.request(.GET, "https://mapi.dompetku.com:8082/promo/get_by_id_category/\(self.categoryID)").responseJSON { response in
                if let JSON = response.result.value {
                    
                    self.promo = JSON as! NSArray
                    self.tableView.reloadData()
                    
                }
            }
        } else {
            Alamofire.request(.GET, "https://mapi.dompetku.com:8082/category").responseJSON { response in
                if let JSON = response.result.value {
                    
                    self.promo = JSON as! NSArray
                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    
}

extension PromoTableViewController {
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.promo.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PromoListCell") as? PromoListCell
        
        print("Isi row : \(self.promo[indexPath.row])")
        
        let item = self.promo[indexPath.row] as! NSDictionary
        
        var urlImage = ""
        if self.isDetail {
            urlImage = "https://mapi.dompetku.com:8082" + (item["url_img"] as! String)
        } else {
            urlImage = (item["bg_image"] as! String)
        }
        
        cell?.imagePromo.af_setImageWithURL(NSURL(string: urlImage)!)

        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 145.0
    }
}

extension PromoTableViewController {
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("select promo \(indexPath.row)")
        let item = self.promo[indexPath.row] as! NSDictionary
        if self.isDetail {
            let showBannerController = self.storyboard?.instantiateViewControllerWithIdentifier("ShowBannerVC") as! ShowBannerController
            let bannerID = item["id"]?.stringValue
            showBannerController.setupData(bannerID!)
            self.navigationController?.pushViewController(showBannerController, animated: true)
        } else {
            let promoTableController = self.storyboard?.instantiateViewControllerWithIdentifier("PromoTableVC") as! PromoTableViewController
            let catID = item["id"]?.stringValue
            promoTableController.setupData(catID, isDetail: true)
            self.navigationController?.pushViewController(promoTableController, animated: true)
            
        }
        
        
    }
}
