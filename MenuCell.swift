//
//  MenuCell.swift
//  isat_idev
//
//  Created by Albert Januar on 12/3/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
}
