//
//  DynamicMenuViewController.swift
//  isat_idev
//
//  Created by Albert Januar on 12/2/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

enum DynamicMenuType {
    case DynamicMenuTypeBuy
    case DynamicMenuTypePay
    case DynamicMenuTypeTransfer
}

class DynamicMenuViewController: UIViewController {
    
    var ID: NSInteger = 0 {
        didSet {
            if self.ID == 50 {
                self.type = .DynamicMenuTypeBuy
            }
            else if self.ID == 51 {
                self.type = .DynamicMenuTypePay
            }
            else if self.ID == 52 {
                self.type = .DynamicMenuTypeTransfer
            }
        }
    }
    var type: DynamicMenuType = .DynamicMenuTypeBuy
    @IBOutlet weak var containerView: UIView!
    var backButton: UIButton!
    var childNavigationController: UINavigationController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clearColor()
        self.view.backgroundColor = UIColor(red: 35.0/255.0, green: 42.0/255.0, blue: 49.0/255.0, alpha: 1.0).colorWithAlphaComponent(0.85)
        
        self.containerView.backgroundColor = UIColor.whiteColor()
        self.containerView.layer.cornerRadius = 5;
        self.containerView.clipsToBounds = true
        
        let tableViewController = MenuTableViewController(ID: self.ID)
        tableViewController.type = self.type
        
        self.childNavigationController = UINavigationController(rootViewController: tableViewController)
        self.childNavigationController.navigationBarHidden = true
        self.childNavigationController.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.backButton = UIButton()
        self.backButton.translatesAutoresizingMaskIntoConstraints = false
        self.backButton.contentEdgeInsets = UIEdgeInsetsMake(8, 16, 8, 8)
        self.backButton.setTitle("Back", forState: UIControlState.Normal)
        self.backButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        self.backButton.addTarget(self, action: #selector(DynamicMenuViewController.backButtonDidTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.containerView.addSubview(self.backButton)
        self.containerView.addSubview(self.childNavigationController.view)
        self.addChildViewController(self.childNavigationController)
        
        let views = ["view": self.childNavigationController.view, "button": self.backButton]
        let viewHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: NSLayoutFormatOptions.AlignAllBaseline, metrics: nil, views: views)
        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]-[button]-8-|", options: NSLayoutFormatOptions.AlignAllLeft, metrics: nil, views: views)
        
        self.containerView.addConstraints(viewHorizontalConstraints)
        self.containerView.addConstraints(verticalConstraints)
    }
}

// MARK: - Custom method
extension DynamicMenuViewController {
    func backButtonDidTapped(sender: AnyObject) {
        if (self.childNavigationController.viewControllers.count == 1) {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            self.childNavigationController.popViewControllerAnimated(true)
        }
    }
}

// MARK: - Class Function
extension DynamicMenuViewController {
    class func showDynamicMenuViewController(presenter: UIViewController, ID: NSInteger) {
        let rootViewController = UIApplication.sharedApplication().delegate?.window!!.rootViewController
        rootViewController!.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        rootViewController!.providesPresentationContextTransitionStyle = true
        rootViewController!.definesPresentationContext = true
        
        let dynamicMenuViewController = DynamicMenuViewController(nibName: "DynamicMenuViewController", bundle: nil)
        dynamicMenuViewController.ID = ID
        dynamicMenuViewController.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        presenter.presentViewController(dynamicMenuViewController, animated: false, completion: nil)
        
        rootViewController!.modalPresentationStyle = UIModalPresentationStyle.FullScreen
        rootViewController!.providesPresentationContextTransitionStyle = false
        rootViewController!.definesPresentationContext = false
    }
}
