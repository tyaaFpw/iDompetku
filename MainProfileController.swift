//
//  MainProfileController.swift
//  isat_idev
//
//  Created by Triyakom PT on 11/9/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MainProfileController: UIViewController, ARPieChartDelegate, ARPieChartDataSource, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var mid_btnview: UIView!
    @IBOutlet var midbtn: UIButton!
    
    @IBOutlet var user_prof_pict: UIImageView!
    @IBOutlet var user_badge: UILabel!
    @IBOutlet var user_msisdn: UILabel!
    @IBOutlet var user_name: UILabel!
    @IBOutlet var user_type: UILabel!
    @IBOutlet var user_xpcoins: UILabel!
    @IBOutlet var user_friend: UILabel!
    @IBOutlet var user_actv_points: UILabel!
    
    @IBOutlet var pieChart: ARPieChart!
    var dataItems: NSMutableArray = []
    @IBOutlet var selectionLabel: UILabel!
    @IBOutlet var val_beli: UILabel!
    @IBOutlet var val_byr: UILabel!
    @IBOutlet var val_trf: UILabel!
    
    @IBOutlet var segment_ctrl: UISegmentedControl!
    @IBOutlet var transaction_1: UIView!
    @IBOutlet var transaction_2: UIView!
    @IBOutlet var myprofile_1: UIView!
    @IBOutlet var myprofile_2: UIView!
    @IBOutlet var friend_view: UIView!
    
    @IBOutlet var friend_segment: UISegmentedControl!
    @IBOutlet var leaderbrd_view: UIView!
    
    @IBOutlet var friend_tableView: UITableView!
    @IBOutlet var friendlist_view: UIView!
    
    
    @IBOutlet weak var totalTransaction: UILabel!
    @IBOutlet var transfer: UILabel!
    @IBOutlet var bayar: UILabel!
    @IBOutlet var beli: UILabel!
    @IBOutlet var saldo: UILabel!
    @IBOutlet var fname: UILabel!
    @IBOutlet var lname: UILabel!
    @IBOutlet var id_type: UILabel!
    @IBOutlet var id_no: UILabel!
    @IBOutlet var dob: UILabel!
    @IBOutlet var gender: UILabel!
    @IBOutlet var mothername: UILabel!
    @IBOutlet var address: UILabel!
    
    @IBOutlet var tr_user_tbv: UITableView!
    
    let objects = JSONObjects()
    var profile_data = Profile_Data()
    var user_tr_data = [User_Transaction]()
    var summary_transaction = Summary_Transaction()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        pieChart.delegate = self
        pieChart.dataSource = self
        self.tr_user_tbv.delegate = self
        self.tr_user_tbv.dataSource = self
        
        self.setInitialViewAppearence()
        self.roundedUIView()
        self.getProfileData()
        self.getUserTransactionData()
        //        self.loginTest()
        print("status: \(self.profile_data.msg)")
        //        self.setProfileData()
        self.friend_tableView.backgroundColor = UIColor.clearColor()
    }
    
    func updatePercent() {
        var percentBuy: Float
        if (self.summary_transaction.num_buy == 0) {
            percentBuy = 0
        }
        else {
            percentBuy = (Float(self.summary_transaction.num_buy)/Float(self.summary_transaction.total_transaction))*100
        }
        var percentPay: Float
        if (self.summary_transaction.num_pay == 0) {
            percentPay = 0
        }
        else {
            percentPay = (Float(self.summary_transaction.num_pay)/Float(self.summary_transaction.total_transaction))*100
        }
        var percentTransfer: Float
        if (self.summary_transaction.num_transfer == 0) {
            percentTransfer = 0
        }
        else {
            percentTransfer = (Float(self.summary_transaction.num_transfer)/Float(self.summary_transaction.total_transaction))*100
        }
        self.val_beli.text = "\(percentBuy) %"
        self.val_byr.text = "\(percentPay) %"
        self.val_trf.text = "\(percentTransfer) %"
        dataItems.addObject(randomItem(percentBuy, red: 51, green: 255, blue: 51))
        dataItems.addObject(randomItem(percentPay, red: 255, green: 51, blue: 51))
        dataItems.addObject(randomItem(percentTransfer, red: 51, green: 153, blue: 255))
        pieChart.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        let maxRadius = min(pieChart.frame.width, pieChart.frame.height)
        print("Max Radius:: \(maxRadius)")
    }
    
    override func viewDidAppear(animated: Bool) {
        self.pieChart.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func roundedUIView() {
        self.mid_btnview.layer.cornerRadius = mid_btnview.frame.size.width/2
        self.midbtn.layer.cornerRadius = midbtn.frame.size.width/2
    }
    
    func setInitialViewAppearence() {
        self.transaction_1.hidden = false
        self.transaction_2.hidden = false
        self.myprofile_1.hidden = true
        self.myprofile_2.hidden = true
        self.friend_view.hidden = true
        self.leaderbrd_view.hidden = true
        self.friendlist_view.hidden = true
    }
    
    @IBAction func segmentedControl(sender: AnyObject) {
        if (self.segment_ctrl.selectedSegmentIndex == 0) {
            self.transaction_1.hidden = false
            self.transaction_2.hidden = false
            self.myprofile_1.hidden = true
            self.myprofile_2.hidden = true
            self.friend_view.hidden = true
            self.friendlist_view.hidden = true
        } else if (self.segment_ctrl.selectedSegmentIndex == 1) {
            self.transaction_1.hidden = true
            self.transaction_2.hidden = true
            self.myprofile_1.hidden = false
            self.myprofile_2.hidden = false
            self.friend_view.hidden = true
            self.friendlist_view.hidden = true
        } else if (self.segment_ctrl.selectedSegmentIndex == 2) {
            self.transaction_1.hidden = true
            self.transaction_2.hidden = true
            self.myprofile_1.hidden = true
            self.myprofile_2.hidden = true
            self.friend_view.hidden = false
            
            if (self.friend_segment.titleForSegmentAtIndex(self.friend_segment.selectedSegmentIndex) == "Daftar Teman") {
                self.leaderbrd_view.hidden = true
                self.friendlist_view.hidden = false
            } else if (self.friend_segment.titleForSegmentAtIndex(self.friend_segment.selectedSegmentIndex) == "Leaderboard") {
                self.leaderbrd_view.hidden = false
                self.friendlist_view.hidden = true
            }
        } else { print("Out of index..") }
    }
    
    @IBAction func friend_segmentedControl(sender: AnyObject) {
        if (self.friend_segment.selectedSegmentIndex == 0) {
            self.leaderbrd_view.hidden = true
            self.friendlist_view.hidden = false
        } else if (self.friend_segment.selectedSegmentIndex == 1) {
            self.leaderbrd_view.hidden = false
            self.friendlist_view.hidden = true
        } else { print("Out of index..") }
    }
    
    func randomColor(red red: Float, green: Float, blue: Float) -> UIColor {
        let randomR: CGFloat = CGFloat((red) / Float(255))
        let randomG: CGFloat = CGFloat((green) / Float(255))
        let randomB: CGFloat = CGFloat((blue) / Float(255))
        return UIColor(red: randomR, green: randomG, blue: randomB, alpha: 1)
    }
    
    func randomInteger(lower: Int, upper: Int) -> Int {
        return Int(arc4random_uniform(UInt32(upper - lower + 1))) + lower
    }
    
    func randomItem(nominal: Float, red: Float, green: Float, blue: Float) -> PieChartItem {
        let value = CGFloat(nominal)
        let color = randomColor(red: red, green: green, blue: blue)
        let description = "\(value)"
        return PieChartItem(value: value, color: color, description: description)
    }
    
    func getCurrentDate() {
    }
    
    func getUserTransactionData() {
        NetraUserProfile.login()
        if let myLoadedString = NSUserDefaults.standardUserDefaults().stringForKey("final_sign") {            self.summary_transaction.num_pay = 0
            self.summary_transaction.num_buy = 0
            self.summary_transaction.num_transfer = 0
            let url = "https://mapi.dompetku.com/webapi/history_transaction"
            let msisdn = "081574448770"
            let params = ["to": "\(msisdn)", "count":"5", "userid":"mobile_apps_elasitas"]
            Alamofire.request(.POST, url, parameters:params).response { request, response, data, error in
                if(data != nil) {
                    let json = JSON(data: data!)
                    for x in json["trxList"].arrayValue {
                        let target = x["agent"].stringValue
                        let nominal = x["amount"].stringValue
                        let timestamp = x["date"].stringValue
                        let type = x["type"].stringValue
                        
                        if (type.containsString("pay")) {
                            self.summary_transaction.num_pay = self.summary_transaction.num_pay + (-1 * Int(nominal)!)
                            self.summary_transaction.total_transaction = self.summary_transaction.total_transaction + (-1 * Int(nominal)!)
                        }
                        else if (type.containsString("buy")) {
                            self.summary_transaction.num_buy = self.summary_transaction.num_buy + (-1 * Int(nominal)!)
                            self.summary_transaction.total_transaction = self.summary_transaction.total_transaction + (-1 * Int(nominal)!)
                        }
                        else if (type.containsString("transfer")) {
                            self.summary_transaction.num_transfer = self.summary_transaction.num_transfer + (-1 * Int(nominal)!)
                            self.summary_transaction.total_transaction = self.summary_transaction.total_transaction + (-1 * Int(nominal)!)
                            
                        }
                        let tr_datas = User_Transaction(target: target, nominal: nominal, timestamp: timestamp, type: type)
                        self.user_tr_data.append(tr_datas)
                    }
                    self.beli.text = self.rupiahFormat(self.summary_transaction.num_buy)
                    self.bayar.text = self.rupiahFormat(self.summary_transaction.num_pay)
                    self.transfer.text = self.rupiahFormat(self.summary_transaction.num_transfer)
                    self.totalTransaction.text = self.rupiahFormat(self.summary_transaction.total_transaction)
                    self.tr_user_tbv.reloadData()
                    self.updatePercent()
                }
            }
        }
    }
    
    func getProfileData() {
        let params = ["userid":"mobile_apps_elasitas", "to":"082112692727"]
        self.objects.shouldDoLogin("balance_check", params: params) { (data: JSON?, error: String?) in
            if let json = data {
                dispatch_async(dispatch_get_main_queue(), {
                    if (json != nil) {
                        self.profile_data = Profile_Data(status: json["status"].stringValue, phone_numb: json["agentData"]["smsAddress"].stringValue, fname: json["agentData"]["firstName"].stringValue, lname: json["agentData"]["lastName"].stringValue, dob: json["agentData"]["dateOfBirth"].stringValue, identity_type: json["agentData"]["idType"].stringValue, identity_no: json["agentData"]["idNo"].stringValue, gender: json["agentData"]["gender"].stringValue, mothername: json["agentData"]["motherMaidenName"].stringValue, address: json["agentData"]["address"].stringValue, balance: json["balance"].stringValue, msg: json["msg"].stringValue)
                        self.setProfileData()
                        self.setProfileInfo()
                    }
                })
            }
        }
    }
    
    func setProfileInfo() {
        self.fname.text = self.profile_data.fname
        self.lname.text = self.profile_data.lname
        self.id_type.text = self.profile_data.identity_type
        self.id_no.text = self.profile_data.identity_no
        self.dob.text = self.profile_data.dob
        self.gender.text = self.profile_data.gender
        self.mothername.text = self.profile_data.mothername
        self.address.text = self.profile_data.address
        self.saldo.text = rupiahFormat(Int(self.profile_data.balance)!)
    }
    
    func rupiahFormat(balance: Int) -> String {
        var currencyFormatter = NSNumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        currencyFormatter.currencyCode = "Rp"
        currencyFormatter.currencyGroupingSeparator = "."
        currencyFormatter.maximumFractionDigits = 0
        return currencyFormatter.stringFromNumber(balance)!
    }
    
    func setProfileData() {
        self.user_name.text = self.profile_data.fname
        self.user_msisdn.text = self.profile_data.phone_numb
    }
    
    //MARK: ARPieChartDelegate
    func pieChart(pieChart: ARPieChart, itemSelectedAtIndex index: Int) {
        let itemSelected: PieChartItem = dataItems[index] as! PieChartItem
        self.selectionLabel.text = "Dari 30 hari lalu"
        let myColorComponents = itemSelected.color.components
    }
    
    //tableView delegate & data source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.user_tr_data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("user_transaction_cell", forIndexPath: indexPath) as? UserTransactionCell
        cell?.transaction_img.image = UIImage(named: "red_circle.png")
        cell?.txt_target.text = self.user_tr_data[indexPath.row].target
        cell?.txt_target_date.text = self.user_tr_data[indexPath.row].timestamp
        cell?.txt_tr_value.text = self.user_tr_data[indexPath.row].nominal
        
        return cell!
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func pieChart(pieChart: ARPieChart, itemDeselectedAtIndex index: Int) {
        //        selectionLabel.text = "No Selection"
        //        selectionLabel.textColor = UIColor.blackColor()
    }
    
    
    //MARK: ARPieChartDataSource
    func numberOfSlicesInPieChart(pieChart: ARPieChart) -> Int {
        return dataItems.count
    }
    
    func pieChart(pieChart: ARPieChart, valueForSliceAtIndex index: Int) -> CGFloat {
        let item: PieChartItem = dataItems[index] as! PieChartItem
        return item.value
    }
    
    func pieChart(pieChart: ARPieChart, colorForSliceAtIndex index: Int) -> UIColor {
        let item: PieChartItem = dataItems[index] as! PieChartItem
        return item.color
    }
    
    func pieChart(pieChart: ARPieChart, descriptionForSliceAtIndex index: Int) -> String {
        let item: PieChartItem = dataItems[index] as! PieChartItem
        return item.description ?? ""
    }
    
    //IBActions
    @IBAction func menuButtonTouched(sender: AnyObject) {
        self.findHamburguerViewController()?.showMenuViewController()
    }
    
    @IBAction func bringBackToHome(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    @IBAction func middleButtonPressed(sender: AnyObject) {
        print("Dompetku..")
    }
    
}

extension UIColor {
    var coreImageColor: CoreImage.CIColor {
        return CoreImage.CIColor(color: self)
    }
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let color = coreImageColor
        return (color.red, color.green, color.blue, color.alpha)
    }
}

/**
 *  MARK: Pie chart data item
 */
public class PieChartItem {
    
    /// Data value
    public var value: CGFloat = 0.0
    
    /// Color displayed on chart
    public var color: UIColor = UIColor.blackColor()
    
    /// Description text
    public var description: String?
    
    public init(value: CGFloat, color: UIColor, description: String?) {
        self.value = value
        self.color = color
        self.description = description
    }
}

struct User_Transaction {
    //var user_id = String()
    //var user_msisdn = String()
    //var id_submenu = String()
    var target = String()
    var nominal = String()
    //var to = String()
    var timestamp = String()
    var type = String()
}

struct Summary_Transaction {
    var num_buy = Int()
    var num_transfer = Int()
    var num_pay = Int()
    var total_transaction = Int()
}

struct Profile_Data {
    var status = String()
    var phone_numb = String()
    var fname = String()
    var lname = String()
    var dob = String()
    var identity_type = String()
    var identity_no = String()
    var gender = String()
    var mothername = String()
    var address = String()
    var balance = String()
    var msg = String()
}
