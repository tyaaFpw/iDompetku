//
//  SideMenuTableCell.swift
//  isat_idev
//
//  Created by Triyakom PT on 11/8/16.
//  Copyright © 2016 Triyakom PTtyaa. All rights reserved.
//

import UIKit

class SideMenuTableCell: UITableViewCell {

    @IBOutlet var menu_image: UIImageView!
    @IBOutlet var menu_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
